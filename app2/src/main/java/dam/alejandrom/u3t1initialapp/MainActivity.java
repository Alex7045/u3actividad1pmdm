package dam.alejandrom.u3t1initialapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private int count;

    private TextView tvDisplay;
    private Button buttonIncrease, buttonDecrease, buttonIncrease2, buttonDecrease2, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonIncrease:
                count++;
                break;
            case R.id.buttonDecrease:
                count--;
                break;
            case R.id.buttonIncrease2:
                count += 2;
                break;
            case R.id.buttonDecrease2:
                count -= 2;
                break;
            case R.id.buttonReset:
                count = 0;
                break;
        }
        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        TextView textViewEx = findViewById(R.id.tvDisplay);
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            String texto = savedInstanceState.getString("");
            textViewEx.setText(texto);
        }
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        TextView textViewEx = findViewById(R.id.tvDisplay);
        super.onSaveInstanceState(outState);
        outState.putString("",textViewEx.getText().toString());
    }
}